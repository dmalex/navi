//
//  ViewController.swift
//  Navi-ios
//  2021 Dmitry Alexandrov
//  Read it:
//  https://ioscoachfrank.com/remove-main-storyboard.html
//
import UIKit
import CoreLocation
import MapKit
import CoreLocation

class ViewController: UIViewController, MKMapViewDelegate, UITextFieldDelegate
{
    let mapView: MKMapView = {
        let control = MKMapView()
        control.layer.masksToBounds = true
        control.layer.cornerRadius = 5
        control.clipsToBounds = false
        control.translatesAutoresizingMaskIntoConstraints = false
        control.showsScale = true
        control.showsCompass = true
        control.showsTraffic = true
        control.showsBuildings = true
        control.showsUserLocation = true
        return control
    }()
    
    
    let startLocation: UITextField = {
        let control = UITextField()
        control.backgroundColor = UIColor.gray
        control.textColor = UIColor.white
        control.placeholder = "From"
        control.layer.cornerRadius = 2
        control.clipsToBounds = false
        control.translatesAutoresizingMaskIntoConstraints = false
        
        control.font = UIFont.systemFont(ofSize: 15)
        control.borderStyle = UITextField.BorderStyle.roundedRect
        control.autocorrectionType = UITextAutocorrectionType.yes
        control.keyboardType = UIKeyboardType.default
        control.returnKeyType = UIReturnKeyType.done
        control.clearButtonMode = UITextField.ViewMode.whileEditing
        control.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        return control
    }()

    
    let finishLocation: UITextField = {
        let control = UITextField()
        control.backgroundColor = UIColor.gray
        control.textColor = UIColor.white
        control.placeholder = "To"
        control.layer.cornerRadius = 2
        control.clipsToBounds = false
        control.translatesAutoresizingMaskIntoConstraints = false
        
        control.font = UIFont.systemFont(ofSize: 15)
        control.borderStyle = UITextField.BorderStyle.roundedRect
        control.autocorrectionType = UITextAutocorrectionType.no
        control.keyboardType = UIKeyboardType.default
        control.returnKeyType = UIReturnKeyType.done
        control.clearButtonMode = UITextField.ViewMode.whileEditing
        control.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        return control
    }()


    let goButton: UIButton = {
        let control = UIButton()
        control.addTarget(self, action: #selector(getYourRoute), for: .touchUpInside)
        control.setTitle("Go!", for: .normal)
        control.backgroundColor = UIColor.blue
        control.titleLabel?.textColor = UIColor.white
        control.layer.cornerRadius = 4
        control.clipsToBounds = false
        control.translatesAutoresizingMaskIntoConstraints = false
        return control
    }()
    
    
    var coordinatesArray = [CLLocationCoordinate2D]()
    var annotationsArray = [MKAnnotation]()
    var overlaysArray = [MKOverlay]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
  
    
    private func setupUI() {
        startLocation.delegate = self
        finishLocation.delegate = self
        mapView.delegate = self

        self.view.addSubview(startLocation)
        self.view.addSubview(finishLocation)
        self.view.addSubview(goButton)
        self.view.addSubview(mapView)
        
        goButton.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        goButton.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        goButton.heightAnchor.constraint(equalToConstant: 34).isActive = true
        goButton.widthAnchor.constraint(equalToConstant: 34).isActive = true
        
        startLocation.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        startLocation.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        startLocation.rightAnchor.constraint(equalTo: goButton.leftAnchor, constant: -10).isActive = true
        
        finishLocation.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        finishLocation.topAnchor.constraint(equalTo: startLocation.bottomAnchor, constant: 10).isActive = true
        finishLocation.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        
        
        mapView.leftAnchor.constraint(equalTo: self.view.leftAnchor, constant: 0).isActive = true
        mapView.topAnchor.constraint(equalTo: finishLocation.bottomAnchor, constant: 10).isActive = true

        mapView.rightAnchor.constraint(equalTo: self.view.rightAnchor, constant: 0).isActive = true
        mapView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
    }

    
    @objc func getYourRoute(_ sender: UIButton) {
        let completion1 = doAfterOne
        
        if self.mapView.annotations.count > 0 {
            self.mapView.removeAnnotations(self.annotationsArray)
            self.annotationsArray = []
        }
        
        if self.overlaysArray.count  > 0 {
            self.mapView.removeOverlays(self.overlaysArray)
            self.overlaysArray = []
        }
        self.coordinatesArray = []
        
        if (self.startLocation.text!.count == 0 || self.finishLocation.text!.count == 0 || self.startLocation.text! == self.finishLocation.text!) {
            return
        }
        
        DispatchQueue.global(qos: .utility).async {
            self.findLocation(location: self.startLocation.text!, showRegion: false, completion: completion1)
        }
    }
    
    
    func doAfterOne() {
        let completion2 = findLocations
        DispatchQueue.global(qos: .background).async {
            self.findLocation(location: self.finishLocation.text!, showRegion: true, completion: completion2)
        }
    }

    
    func findLocations() {
        if self.coordinatesArray.count < 2 {
            return
        }
        let markLocationOne = MKPlacemark(coordinate: self.coordinatesArray.first!)
        let markLocationTwo = MKPlacemark(coordinate: self.coordinatesArray.last!)
        let directionRequest = MKDirections.Request()
        directionRequest.source = MKMapItem(placemark: markLocationOne)
        directionRequest.destination = MKMapItem(placemark: markLocationTwo)
        directionRequest.transportType = .automobile
        
        let directions = MKDirections(request: directionRequest)
        directions.calculate { (response, error) in
            if error != nil {
                print(String(describing: error))
            } else {
                let myRoute: MKRoute? = response?.routes.first
                if let a = myRoute?.polyline {
                    if self.overlaysArray.count > 0 {
                        self.mapView.removeOverlays(self.overlaysArray)
                        self.overlaysArray = []
                    }
                    self.overlaysArray.append(a)
                    self.mapView.addOverlay(a)
                    self.mapView.centerCoordinate = self.coordinatesArray.last!
                    
                    let span = MKCoordinateSpan(latitudeDelta: 0.9, longitudeDelta: 0.9)
                    let region = MKCoordinateRegion(center: self.coordinatesArray.last!, span: span)
                    self.mapView.setRegion(region, animated: true)
                }
            }
        }
    }
    
    
    func findLocation(location: String, showRegion: Bool = false, completion: @escaping () -> Void) {
        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(location) { (placemarks, error) in
            if let placemark = placemarks?.first {
                let coordinates = placemark.location!.coordinate
                self.coordinatesArray.append(coordinates)
                let point = MKPointAnnotation()
                point.coordinate = coordinates
                point.title = location
                
                if let country = placemark.country {
                    point.subtitle = country
                }
                self.mapView.addAnnotation(point)
                self.annotationsArray.append(point)
                
                if showRegion {
                    self.mapView.centerCoordinate = coordinates
                    let span = MKCoordinateSpan(latitudeDelta: 0.9, longitudeDelta: 0.9)
                    let region = MKCoordinateRegion(center: coordinates, span: span)
                    self.mapView.setRegion(region, animated: false)
                }
            } else {
                print(String(describing: error))
            }
            completion()
        }
    }

    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let polylineRenderer = MKPolylineRenderer(overlay: overlay)
        if overlay is MKPolyline {
            polylineRenderer.strokeColor = UIColor.green
            polylineRenderer.lineWidth = 4
        }
        return polylineRenderer
    }
}
